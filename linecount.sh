#!/bin/sh
echo 'CoffeeScript'
find . -name '*.coffee' | xargs wc -l
echo
echo
echo 'Python'
find . -name '*.py' | xargs wc -l
echo
echo
echo 'HTML'
find . -name '*.html' | xargs wc -l
echo
echo
find . -name '*.coffee' -or -name '*.py' -or -name '*.html' | xargs wc -l | grep total
