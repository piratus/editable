# coding: utf-8
"""Database helpers"""
from bson import ObjectId
from flask.ext.pymongo import PyMongo
from werkzeug.exceptions import abort


mongo = PyMongo()


class Document(object):
    """Base class for documents"""
    def __init__(self, **kwargs):
        self._data = kwargs or {}

    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, value):
        self._data[key] = value

    def __contains__(self, key):
        return key in self._data

    @classmethod
    def _collection(cls):
        """Mongo collection object for current document"""
        collection_name = '{}s'.format(cls.__name__.lower())
        return mongo.db[collection_name]

    @classmethod
    def all(cls, **kwargs):
        return cls._collection().find(kwargs)

    @classmethod
    def get(cls, **kwargs):
        """Find a document in collection"""
        if 'id' in kwargs:
            _id = ObjectId(kwargs.pop('id'))
            kwargs['_id'] = _id

        obj = cls._collection().find_one(kwargs)
        if obj is None:
            return None

        return cls(**obj)

    @classmethod
    def get_or_404(cls, **kwargs):
        result = cls.get(**kwargs)
        if not result:
            abort(404)

        return result

    @property
    def id(self):
        if not '_id' in self:
            return None

        return str(self['_id'])

    def save(self):
        """Save the document to collection"""
        return self._collection().save(self._data)

    def to_dict(self):
        return {key: value for key, value in self._data.iteritems()
                if key != '_id'}

