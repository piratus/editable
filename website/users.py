# coding: utf-8
"""User management views and functions"""
import hashlib
from flask import (Blueprint, current_app, session, g, request, flash, redirect,
                   url_for, render_template, jsonify)
from .database import Document


users = Blueprint('users', __name__)


def password_hash(password):
    """Create masked version of the password"""
    hasher = hashlib.md5(current_app.secret_key)
    hasher.update(password)
    return hasher.hexdigest()


class User(Document):
    """User model"""

    def is_valid_password(self, password):
        """Check password for user"""
        return self['password'] == password_hash(password)

    def __repr__(self):
        return '<User:{}>'.format(self['username'])


@users.before_app_request
def init_user():
    """Add user object to global context if user_id is found in session"""
    if 'user_id' in session:
        g.user = User.get(id=session['user_id'])
    else:
        g.user = None


@users.app_context_processor
def inject_user():
    """Add user info to template context"""
    return {
        'user': g.user,
        'is_admin': g.user is not None
    }


@users.route('/login/', methods=['GET', 'POST'])
def login():
    """Check username and password and log in user if correct"""
    if request.method == 'POST':
        user = User.get(username=request.form['username'])
        if not user or not user.is_valid_password(request.form['password']):
            flash('Username or password is incorrect')
            return redirect(url_for('.login'))

        session['user_id'] = user.id
        g.user = user

        return redirect(url_for('index'))

    return render_template('login.html')


@users.route('/logout/')
def logout():
    """Log user out and redirect to login page"""
    if 'user_id' in session:
        del session['user_id']

    return redirect(url_for('.login'))


def login_required(func):
    """Decorator for authentication enforcing"""
    def wrapper(*args, **kwargs):
        if g.user is not None:
            return func(*args, **kwargs)

        if request.is_xhr:
            return jsonify(success=False), 401

        return redirect(url_for('.login'))

    return wrapper
