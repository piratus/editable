# coding: utf-8
"""Editable website"""
from flask import Flask

from .database import mongo
from .api import api
from .users import users, login_required
from .pages import show_page, inject_pages_template_context


app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'development secret'
app.config['MONGO_DBNAME'] = 'pages'
mongo.init_app(app)


app.context_processor(inject_pages_template_context)
app.add_url_rule('/', 'index', show_page, defaults={'url': ''})
app.add_url_rule('/<path:url>', 'page', show_page)


app.register_blueprint(users, url_prefix='/users')
app.register_blueprint(api, url_prefix='/api')
