define (require)->
  templates = require 'templates'
  Backbone = require 'backbone'
  {HtmlBlock} = require 'block'

  class Section extends Backbone.View
    controlsTemplate: templates.get 'section_controls_template'

    events:
      'click [data-action=add-section]': 'addNewBlock'

    initialize: ->
      @sectionId = @$el.attr('data-section')

      for blockEl in @$el.find '[data-block]'
        new HtmlBlock el: blockEl

      @controls = $ @controlsTemplate()
      @$el.append @controls

    addNewBlock: (event)->
      type = $(event.currentTarget).attr 'data-value'
      $.ajax "/api/blocks/#{window.PAGE_ID}/",
        type: 'POST'
        data:
          type: type
          section: @sectionId
        success: (response)=>
          newBlock = $ response.html
          @controls.before newBlock
          new HtmlBlock el: newBlock

  return {
    Section: Section
  }
