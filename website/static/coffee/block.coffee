define (require)->
  Backbone = require 'backbone'
  editor = require 'editor'

  class HtmlBlock extends Backbone.View
    events:
      'click [data-block-command=edit]': 'show'
      'click [data-block-command=remove]': 'remove'

    initialize: ->
      console.log @$el, @$el.attr 'data-block'
      @blockId = @$el.attr 'data-block'
      @url = "/api/blocks/#{window.PAGE_ID}/#{@blockId}/"
      @editor = new editor.HtmlEditor
        el: @$el.find '[data-editable=text]'
        url: @url

      @blockControls = @$el.find 'block_controls'

    show: ->
      @editor?.show()

    remove: ->
      if confirm 'Are you shure, you want to delete this block?'
        $.ajax @url,
          type: 'DELETE'
          success: =>
            @$el.remove()

  return {
    HtmlBlock: HtmlBlock
  }
