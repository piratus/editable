define (require)->
  templates = require 'templates'
  Backbone = require 'backbone'

  class Settings extends Backbone.View
    windowTemplate: templates.get 'settings-template'
    buttonTemplate: templates.new '<a class="settings-btn"><i class="icon-cogs icon-2x"></i></a>'

    initialize: ->
      @window = $(@windowTemplate())
      @button = $(@buttonTemplate())

      @window.on 'click', '.close', @hide
      @button.on 'click', @show

      $('body')
        .append(@window)
        .append(@button)

    show: =>
      @button.hide()
      @window.show speed: 100

    hide: =>
      @window.hide speed: 100
      @button.show()

  return {
    Settings: Settings
  }
