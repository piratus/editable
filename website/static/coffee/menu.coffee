define (require)->
  Backbone = require 'backbone'
  templates = require 'templates'

  class MenuItem extends Backbone.View
    events:
      'click [data-command=remove-item]': 'remove'

    initialize: ->
      @itemId = @$el.attr 'data-id'
      @$el
        .addClass('editable-menuitem')
        .find('a').append $ '<i class="icon-remove" data-command="remove-item"></i>'

    remove: ->
      $.ajax "/api/menuitems/#{@options.alias}/#{@itemId}/",
        type: 'DELETE',
        success: =>
          @$el.remove()
      false

  class Menu extends Backbone.View
    formTemplate: templates.get 'add-menu-item-template'

    events:
      'click [data-command=add-item-form]': 'showAddForm'
      'click [data-command=add-item]': 'addItem'

    initialize: ->
      @alias = @$el.attr 'data-alias'

      for itemEl in @$el.find('li')
        new MenuItem alias: @alias, el: itemEl

      @addButton = $ '<li><a data-command="add-item-form"><i class="icon-plus"></i></a></li>'
      @$el.append @addButton

      @dialog = $ @formTemplate()
      @$el.append @dialog

    showAddForm: (event)->
      @dialog.modal 'show'
      setTimeout (=> @dialog.find('[name=title]').focus()), 100

    addItem: ->
      form = @dialog.find 'form'
      title = form.find('[name=title]').val()
      url = form.find('[name=url]').val()

      if not title or not url
        return

      $.ajax "/api/menuitems/#{@alias}/",
        type: 'POST'
        data:
          title: title
          url: url
        success: ->
          location.href = form.find('[name=url]').val()
      $.ajax


  return {
    Menu: Menu
  }
