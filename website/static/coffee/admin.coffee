define (require)->
  {Menu} = require 'menu'
  editor = require 'editor'
  {Section} = require 'sections'
  {HtmlBlock} = require 'block'
  {Settings} = require 'settings'
  templates = require 'templates'

  return {
    initialize: ->
      for menuEl in $ '.menu-container'
        menu = new Menu el: menuEl

      for sectionEl in $ '[data-section]'
        section = new Section el: sectionEl

      for editableEl in $ '[data-editable=simple]'
        editable = new editor.ElementEditor el: editableEl

      window.settings = new Settings()
  }
