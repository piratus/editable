TEMPLATES = {}

define (require)->
  _ = require 'underscore'

  load = ->
    for scriptBlock in $ 'script[type="text/html"]'
      scriptBlock = $ scriptBlock
      TEMPLATES[scriptBlock.attr 'id' ] = _.template scriptBlock.html()
      scriptBlock.remove()

  load()

  return {
    load: load,

    get: (id)->
      TEMPLATES[id]

    new: (text)->
      _.template text
  }
