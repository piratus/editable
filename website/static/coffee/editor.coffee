define (require) ->
  wysihtml5 = require 'lib/wysihtml5'
  templates = require 'templates'
  Backbone = require 'backbone'

  ParserRules =
    classes:
      'wysiwyg-text-align-right': 1
      'wysiwyg-text-align-center': 1
      'wysiwyg-text-align-left': 1
    tags:
      h1: 1, h2: 1, h3: 1, p: 1,
      div: 1, ol: 1, ul: 1, li: 1,
      b: 1, i: 1, u: 1

  nextEditorId = do ->
    id = 1
    return ->
      id += 1

  class ElementEditor extends Backbone.View
    className: 'editable'

    events:
      'click': 'startEdit'
      'blur': 'stopEdit'

    initialize: ->
      @paramName = @$el.attr 'data-name'
      @url = @$el.attr 'data-url'
      @oldVal = null

      @$el.addClass 'editable single-line'
      @$el.attr 'contenteditable', false

      if @$el.innerHeight() is 0
        @$el.innerHeight @$el.css 'lineHeight'

    getValue: =>
      @$el.html()

    setValue: (value)=>
      @$el.html value

    checkClick: (event)=>
      if $(event.target).closest(@$el).length is 0
        @stopEdit yes

    startEdit: =>
      return false if @$el.hasClass 'editing'

      $(document).on 'keydown', @el, @keydown
      $(document).on 'click', @checkClick

      @oldVal = @getValue()

      @$el.addClass 'editing'
      @$el.attr 'contenteditable', 'true'

    stopEdit: (revert=no)=>
      return false if not @$el.hasClass 'editing'

      $(document).off 'keydown', @keydown
      $(document).off 'click', @checkClick


      @$el.removeClass 'editing'
      @$el.attr('contenteditable', 'false')

      if revert and @oldVal isnt null
        @setValue @oldVal

      @oldVal = null

    save: ->
      oldValue = @oldVal
      data = {}
      data[@paramName] = @getValue()

      $.ajax @url,
        type: 'PUT'
        data: data
        success: =>
          @oldVal = @getValue()
        error: =>
          @setValue oldValue

    keydown: (event)=>
      if event.which is 27
          @stopEdit yes
      else if event.which is 13
          @save()
          @stopEdit()
          return false

  class HtmlEditor extends Backbone.View
    template: templates.get 'html-editor-template'

    events:
      'click': 'show'
      'click [data-command=save]': 'save'
      'click [data-command=insertImage]': 'insertImage'
      'click [data-command=createLink]': 'createLink'

    initialize: ({@url})->
      @editorId = nextEditorId()

      content = @$el.html()
      @$el.html ''
      @content = $("<div class='editable'>#{content}</div>").appendTo @el

      data =
        id: @editorId
        height: @$el.innerHeight()
        width: @$el.innerWidth()

      @wrapper = $(@template data).insertAfter(@content).hide()
      @editor = null

    getEditor: =>
      @editor ?= new wysihtml5.Editor "editor-#{@editorId}",
        toolbar: "editor-#{@editorId}-toolbar"
        parserRules: ParserRules
        stylesheets: ['/static/css/bootstrap.css', '/static/css/app.css']
        useLineBreaks: no
        cleanup: yes

    checkClick: (event)=>
      if $(event.target).closest(@$el).length is 0
        @hide()

    show: ->
      editor = @getEditor()
      editor.setValue @content.html()

      @content.hide()
      @wrapper.show()

      $(document).on 'click', @checkClick
      setTimeout (-> editor.focus()), 100

    hide: ->
      @wrapper.hide()
      @content.show()
      $(document).off 'click', @checkClick

    save: ->
      value = @getEditor().getValue()

      $.ajax @url,
        type: 'PUT'
        data:
          content: value
        success: =>
          @content.html value
          @hide()
        error: ->
          alert 'Save failed'

    insertImage: ->
      alert 'not yet'

    createLink: ->
      alert 'not yet'

  return {
    ElementEditor: ElementEditor,
    HtmlEditor: HtmlEditor
  }
