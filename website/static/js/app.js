requirejs.config({
  baseUrl: '/static/js',
  paths: {
    jquery: 'lib/jquery',
    underscore: 'lib/underscore',
    backbone: 'lib/backbone'
  },
  shim: {
    'lib/bootstrap': ['jquery']
  }
});

requirejs(['jquery', 'lib/bootstrap', 'admin'], function ($, bootstrap, admin) {

  $(function () {
    admin.initialize()
  });

});
