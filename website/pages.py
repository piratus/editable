# coding: utf-8
"""Page model and view"""
import random
from string import ascii_lowercase, digits
from flask import abort, render_template, get_template_attribute
from .database import Document


CONTENT_SECTION = 'content'
ID_CHARS = ascii_lowercase + digits


def generate_id(size=4, exclude=None):
    exclude = exclude or {}

    new_id = None
    while not new_id or new_id in exclude:
        new_id = ''.join(random.choice(ID_CHARS) for x in xrange(size))

    return new_id


def get_default_content(block_type):
    return '<p>new html block</p>'


class Page(Document):

    def __init__(self, url, **kwargs):
        kwargs['url'] = url
        kwargs.setdefault('blocks', [])
        kwargs.setdefault('title', '')
        kwargs.setdefault('published', False)
        super(Page, self).__init__(**kwargs)

    def __repr__(self):
        return '<Page:{}>'.format(self['url'])

    @property
    def blocks(self):
        return self['blocks']

    def add_block(self, block_type, section=None):
        block = {
            'id': generate_id(exclude={b.get('id') for b in self['blocks']}),
            'type': block_type,
            'section': section or CONTENT_SECTION,
            'content': get_default_content(block_type),
            'order': len(self['blocks']) + 1
        }
        self['blocks'].append(block)

        return block

    def get_block(self, block_id):
        try:
            return next(b for b in self['blocks'] if b['id'] == block_id)
        except StopIteration:
            raise KeyError('No such block "{}"'.format(block_id))

    def remove_block(self, block_id):
        self['blocks'].remove(self.get_block(block_id))

    def get_blocks_for_section(self, section):
        return [b for b in self['blocks'] if b['section'] == section]


class Menu(Document):

    def __init__(self, alias, **kwargs):
        kwargs['alias'] = alias
        kwargs.setdefault('items', [])
        super(Menu, self).__init__(**kwargs)

    def add_item(self, title='', url=''):
        item = {
            'id': generate_id(exclude={i.get('id') for i in self['items']}),
            'title': title,
            'url': url,
            'order': len(self['items']) + 1
        }
        self['items'].append(item)

        return item

    def get_item(self, item_id):
        try:
            item = next(i for i in self['items'] if i['id'] == item_id)
        except StopIteration:
            raise KeyError('No such item "{}"'.format(item_id))

        return item

    def remove_item(self, item_id):
        item = self.get_item(item_id)
        self['items'].remove(item)

        return item


def show_page(url):
    url = '/' + url
    page = Page.get(url=url)

    if not page and url == '/':
        page = Page(url, title='Index page')
        page.save()

    if not page:
        abort(404)

    return render_template('index.html', page=page)


def render_block(page, block, is_admin=True):
    renderer = get_template_attribute('_macros.html', 'page_block')
    return renderer(page=page, block=block, is_admin=is_admin)


def render_menu(alias_or_menu, force=False, horizontal=True):
    if isinstance(alias_or_menu, Menu):
        menu = alias_or_menu
    else:
        menu = Menu.get(alias=alias_or_menu)
        if not menu and force:
            menu = Menu(alias=alias_or_menu)
            menu.save()

    if menu:
        return render_template('blocks/menu.html', menu=menu,
                               horizontal=horizontal)

    return ''


def inject_pages_template_context():
    return {
        'render_menu': render_menu,
        'menus': {m['alias']: m for m in Menu.all()}
    }
