# coding: utf-8
"""Page manipulation API"""
from flask import Blueprint, request, jsonify
from flask.views import MethodView
from .users import login_required
from .pages import Page, Menu, render_block, render_menu


api = Blueprint('api', __name__)


class MenuItemResource(MethodView):
    decorators = [login_required]

    def get(self, alias):
        menu = Menu.get_or_404(alias=alias)
        return jsonify(success=True, menu=menu.to_dict())

    def post(self, alias):
        menu = Menu.get_or_404(alias=alias)

        item_title = request.form.get('title')
        item_url = request.form.get('url')

        if not item_title or not item_url:
            return jsonify(success=False)

        item = menu.add_item(title=item_title, url=item_url)
        menu.save()

        if not Page.get(url=item_url):
            page = Page(item_url, title=item_title)
            page.save()

        html = u'<li data-id="{id}"><a href="{url}">{title}</a></li>'.format(**item)

        return jsonify(success=True, html=html)

    def put(self, alias, item_id):
        pass

    def delete(self, alias, item_id):
        menu = Menu.get_or_404(alias=alias)

        try:
            item = menu.remove_item(item_id)
        except KeyError:
            return jsonify(success=False)

        menu.save()

        return jsonify(success=True)


menus_view = MenuItemResource.as_view('menu')
api.add_url_rule('/menuitems/<alias>/', view_func=menus_view, methods=['POST'])
api.add_url_rule('/menuitems/<alias>/<item_id>/', view_func=menus_view,
                 methods=['GET', 'PUT', 'DELETE'])


class PagesResource(MethodView):
    decorators = [login_required]

    def put(self, page_id):
        page = Page.get_or_404(id=page_id)
        for key, value in request.form.iteritems():
            page[key] = value

        page.save()

        return jsonify(success=True)


pages_view = PagesResource.as_view('pages')
api.add_url_rule('/pages/<page_id>/', view_func=pages_view, methods=['PUT'])


class BlocksResource(MethodView):
    decorators = [login_required]

    def get(self, page_id, block_id):
        page = Page.get_or_404(id=page_id)

        try:
            block = page.get_block(block_id)
        except KeyError:
            return jsonify(success=False)

        return jsonify(success=True, html=render_block(page, block))

    def post(self, page_id):
        assert 'type' in request.form

        page = Page.get_or_404(id=page_id)

        block_type = request.form['type']
        section_id = request.form.get('section')

        new_block = page.add_block(block_type, section_id)
        page.save()

        return jsonify(success=True, html=render_block(page, new_block))

    def put(self, page_id, block_id):
        page = Page.get_or_404(id=page_id)

        if not 'content' in request.form:
            return jsonify(success=False), 400

        try:
            block = page.get_block(block_id)
        except KeyError:
            return jsonify(success=False)

        block['content'] = request.form['content']
        page.save()

        return jsonify(success=True)

    def delete(self, page_id, block_id):
        page = Page.get_or_404(id=page_id)

        try:
            page.remove_block(block_id)
        except KeyError:
            return jsonify(success=False)

        page.save()

        return jsonify(success=True)


blocks_view = BlocksResource.as_view('blocks')
api.add_url_rule('/blocks/<page_id>/<block_id>/', view_func=blocks_view,
                 methods=['GET', 'PUT', 'DELETE'])
api.add_url_rule('/blocks/<page_id>/', view_func=blocks_view, methods=['POST'])
