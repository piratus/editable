# An editable website


## Prerequisites
1. Python and virtualenv

    Tested on python version 2.7. Virtualenv can be installed through `easy_install virtualenv`

1. MongoDB

    A suitable installation package can be found on the [downloads](http://www.mongodb.org/downloads) page. If you are on OS X and using Homebrew, you can type:

        $ brew install mongodb


## Installation instructions
2. Clone this repo

        git clone https://bitbucket.org/piratus/editable.git

2. Create and activate a virtual env

        $ virtualenv editable_env
        $ source editable_env/bin/activate

    Or if on widows:

        D:\>virtualenv editable_env
        D:\>editable_env\Scripts\activate.bat

2. Install dependencies

        $ pip install -r requirements.txt

3. Add an admin user

        $ python manage.py add_user piratus qwerty

3. Run the development server

        $ python manage.py runserver
