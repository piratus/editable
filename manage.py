# coding: utf-8
"""Development helpers"""
import sys

from flask.ext.script import Manager
from website import app, mongo
from website.users import password_hash


manager = Manager(app)


@manager.command
def add_user(username, password):
    """Add a new superuser."""
    if mongo.db.users.find_one({'username': username}):
        print >> sys.stderr, 'Error: User "{}" already exists'.format(username)
        sys.exit(1)

    user = {'username': username, 'password': password_hash(password)}
    mongo.db.users.insert(user)

    print 'User "{}" successfully created'.format(username)


if __name__ == '__main__':
    manager.run()
